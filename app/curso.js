// Script relativo ao controle da pagina curso.html
var app = angular.module("AppCurso", []);

// Metodo de controle da pagina:
app.controller("CursoCtrl", function ($scope, $http) {

    // Funcao que ira iniciar com a pagina e obter o id do curso pelo sessionStorage.
    $scope.init = function () {
        $scope.idCurso = window.sessionStorage.getItem('idCurso');
    }
    $scope.init();

    // Tentando realizar conexao com o servidor:
    $http({
        method: 'GET',
        url: 'https://cefis.com.br/api/v1/event/' + $scope.idCurso + '?include=classes'
        // Conexao foi bem sucedida:
    }).then(function (response) {
        $scope.curso = response.data.data;
        $scope.aulas = $scope.curso.classes;
        $scope.comoFunciona = 'O Curso é estruturado em Vídeo Aulas com foco e abordagem prática para você assistir onde quiser e quantas vezes desejar. Além das vídeo aulas você poderá enviar dúvidas ao professor, fazer download de apostilas e materiais complementares. Para completar, ao final do curso você poderá testar seus conhecimentos e gerar um certificado reconhecido em todo território nacional.';
        $scope.incluso = 'Aula para reassistir o curso quantas vezes quiser + Tira dúvidas com o instrutor + Download Material + Certificado';
        // Conexao obteve algum erro.    
    }, function (error) {
        console.log('Erro ao tentar realizar conexao com servidor.');
    });

});