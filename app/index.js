// Script relativo ao controle da pagina index.html
var app = angular.module("AppCursos", ['angularUtils.directives.dirPagination']);

// Metodo de controle da pagina:
app.controller("CursosCtrl", function ($scope, $http) {

    // Tentando realizar conexao com o servidor:
    $http({
        method: 'GET',
        url: 'https://cefis.com.br/api/v1/event'
        // Conexao foi bem sucedida:
    }).then(function (response) {
        $scope.cursos = response.data.data;
        // Conexao obteve algum erro:    
    }, function (error) {
        console.log('Erro ao tentar realizar conexao com servidor.');
    });

    // Funcao que realiza a acao do botao de ver os detalhes de um curso.
    $scope.verCurso = function (idCurso) {
        window.sessionStorage.setItem('idCurso', idCurso);
        window.location.href = 'curso.html';
    }

});